
DROP TABLE IF EXISTS authorities;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS pizza;
DROP TABLE IF EXISTS drink;

CREATE TABLE users (
	username varchar(50) NOT NULL,
    firstName varchar(50) NOT NULL,
    secondName varchar(50) NOT NULL,
    phoneNumber varchar(50) NOT NULL,
    address varchar(50) NOT NULL,
	email varchar(50) NOT NULL,
	password varchar(100) NOT NULL,
	enabled int NOT NULL,
	PRIMARY KEY (username)
) ;

CREATE TABLE authorities (
	username varchar(50) NOT NULL,
	authority varchar(50) NOT NULL,
	UNIQUE (username,authority),
	CONSTRAINT authorities_ibfk_1 FOREIGN KEY (username) REFERENCES
	users (username) ON DELETE CASCADE ON update cascade
);

CREATE TABLE pizza(
	id SERIAL PRIMARY KEY,
    info VARCHAR(45) NOT NULL,
    size INTEGER NOT NULL,
    price INTEGER NOT NULL
);


CREATE TABLE orders(
	id SERIAL PRIMARY KEY,
	status INTEGER default 0,
	productType VARCHAR(16) NOT NULL,
    idProduct INTEGER NOT NULL,
    idClient varchar(50) NOT NULL,
    CONSTRAINT FK_orders_idClient FOREIGN KEY (idClient) REFERENCES users (username) ON DELETE CASCADE ON update cascade
);

INSERT INTO users VALUES
		('test', 'Alex','M','+1256','Skopje','test@mail.com' ,'{bcrypt}$2a$10$KmLR.FvUUD4zAqS3ls4nHOM2IJTk8rmdoKNEQOND6CNmq.xq628PK',1), -- test
		('admin', 'Kate','S','+1234','Skopje','admin@pizza.com' ,'{bcrypt}$2a$10$NKXD6KiFzHlowPttT1AEheiH8XvCY8Mjt6CLovyyDEJVJntu5C.wW',1); -- admin

INSERT INTO authorities VALUES
			('test','ROLE_USER'),
			('admin','ROLE_USER'),
			('admin','ROLE_ADMIN');

INSERT INTO pizza VALUES (1,'vegetarian', 35, 85), (2,'margarita', 35, 70), (3,'chicken', 35, 75), (4,'tuna', 35, 65);
INSERT INTO orders VALUES (default,0,'pizza',1,'test'), (default ,0,'pizza',2,'test');
INSERT INTO orders VALUES (default,0,'pizza',4,'admin'), (default ,0,'pizza',3,'test')
