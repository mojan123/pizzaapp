# PizzaApp
Programming test for pizza app
#### version: __1.0.0__

### Get started

1. Download or Clone project:
    ```
   git clone https://mojan123:He3+He3=He4+2p@bitbucket.org/mojan123/pizzaapp.git
    ``` 
2. in docker folder run: sudo docker-compose up
3. find the ip of postgres instance sudo docker inspect docker_postgresql_1 | grep IPAddress
4. insert the ip address as postgres in /etc/hosts
5. Run mvn spring-boot:run



### Default user

user:     admin <br>
password: admin

user:     test <br>
password: test
