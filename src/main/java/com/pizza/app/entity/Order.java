package com.pizza.app.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Order {

    private int id;
    private int status;

    private Pizza pizza;
    private User user;

    public String getProductName() {
        return pizza.getInfo();
    }

    public void setUsername(String username) {
        this.user = new User();
        this.user.setUsername(username);
    }
}
